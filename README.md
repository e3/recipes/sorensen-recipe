# sorensen conda recipe

Home: "https://gitlab.esss.lu.se/epics-modules/sorensen"

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS sorensen module
